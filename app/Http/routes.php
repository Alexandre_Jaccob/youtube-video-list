<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', "VideoController@index");
Route::get('video/verification', 'VideoController@verification');
Route::get('video/getlist', 'VideoController@returnVideosAsJson');
Route::get('video/getEmbededCode', 'VideoController@getPlayerById');
Route::resource('video', 'VideoController');

