# README #

### What is this repository for? ###

Ths Repostitory contains a demo project for a list of youtube videos.

### How do I get set up? ###

####Dependencies
* an Unix server
* php5
* php5-curl
* apache
  * mod_rewrite
* composer is recommended
####Get it working
* Clone the reposity in your document root or somewhere else and link it to the document root
* Enable the module rewrite in apache 

```
#!bash

 a2enmod mod_rewrite
```
* and activate it for the directory in in the vhost file

```

#!apache


        <Directory "/path/to/folder">
                AllowOverride All
                RewriteEngine On
        </Directory>

```
** !! Be aware that this allows the local .htacces to overwrite anything, please read the apache2 ducomentation for further details !! **
####Important configuration files
* .env and phpunit.xml are in the root directory of the project
* database.php lies in the config folder.
####Database
* Create a database and a user which can create tables, read and write to the database
* choose utf8_unicode_ci as kollation
* Update the .env file with these values.
* **Note:** The database.php values are fall backs
* **Note: ** The pre configured SQLite connection is used for unit tests. 
       * If you want to use it for something else change the phpunit.xml file and edit the entry <env name="DB_CONNECTION" value="sqlite"/>. 
       * Change the value attribute to the connection which the unit test shall use instead of. You may have to create a new connection in database.php.
####Migrations
 * The Migrations will create the tables for you.
 * After you created the database and updated the .env file, run php artisan migrate in the root directory of the project and artisan creates the tables.
### Unit Testing ###
* Run the unit test from the root directory of the project with: phpunit tests/file.php


### How does it work? ###
####Technologies
* Aside from the comments in the code, which explains what the methods do, here is explained which mechansim are used.
* The PHP-Framework is Laravel5 with MySQL Database, for unit testing an in memory SQLite Database is used.
* As JavaScript Framework Angular JS is used.
####Structure
* You find Controllers in app/Http/Controllers, the only Controller used is the VideoController
* Views can be found in ressources/views
* Models can be found directly in the app directory
* Only the Video Model is used and only inherits its methods, it only inherits to define a table which will be used
* The table is the plural form of the model name
####what does the JavaScript do?
* The Project uses Ajax for Communication with JSON as communication protocol.
* Angular JS creates the video list dynamically, after it loads a JSON from the server with the video data
* If you add a video to the database, the video data is returned by the server and added to first position of the angular model.
* Validation happens on keyup, Angluar sends the URL to the server and the server returns a validation message.
####Protocol
#####Add a new Video
Post to the server a JSON with following structure to /video:

```
#!json

{
"_token": "CSRF_TOKEN",
"url": "URL",
"_methode": "POST"
}
```
The Server will respond:

```
#!JSON
{
"title":"Title",
"duration": "00:07:01",
"description":"Description of the Video",
"embededHtml": "Thumbnail HTML",
"youtubeid": "WEQnzs8wl6E"
}
```
In Case of Failure:

```
#!JSON

{
"verification": false, 
"message" : "error message"
}
```
#####List of all Videos 
Send a GET Request to video/getlist to get the list of all videos.
The server will send a JSON Array like this:
```
#!JSON
[
{
"title":"Title",
"duration": "00:07:01",
"description":"Description of the Video",
"embededHtml": "Thumbnail HTML",
"youtubeid": "WEQnzs8wl6E"
}, … 
]
```

#####Verification
Send a GET Request to /video/verification.


```
#!JSON
{
"_token": "CSRF_TOKEN",
"url":"youtube URL"
}
```
Repsonse:
```
#!JSON

{
"verification": true 
}
```
Or in Case of failure:
```
#!JSON

{
"verification": false, 
"message" : "error message"
}
```

### Who do I talk to? ###

* Dennis Beier, nanovim@gmail.com